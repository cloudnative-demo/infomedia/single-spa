import React from "react";
// helpers
import Components from "./components/helper";
// icons
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import PersonIcon from "@mui/icons-material/Person";
import LockIcon from "@mui/icons-material/Lock";
//import ReCAPTCHA from "react-google-recaptcha";
import theme from "./components/theme";
const SignInSchema = Components.Yup.object().shape({
  username: Components.Yup.string().required("User Id required"),
  password: Components.Yup.string().required("Password required"),
  // .matches(
  //   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
  //   "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
  // ),
});

function Root(props) {
  const [loading, setLoading] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [message, setMessage] = React.useState(false);
  const [showPassword, setShowPassword] = React.useState(false);
  const loadRecaptchaV3 = (callback) => {
    const existingScript = document.getElementById("recaptcha");
    if (!existingScript) {
      const script = document.createElement("script");
      script.src = `https://www.google.com/recaptcha/api.js?render=6Lcnu3EdAAAAAObE6a1sU-41rpoX8yDczbu9529P`;
      script.id = "recaptcha";
      script.async = true;
      script.defer = true;
      script.preload = true;
      document.head.appendChild(script);
      script.onload = () => {
        if (callback) callback();
      };
    }
    if (existingScript && callback) callback();
  };
  const handleLogin = (params) => {
    setLoading(true);
    const axios = window.axios.default;
    axios
      .post(`/auth/login`, params)
      .then((res) => {
        if (res) {
          setLoading(false);
          const storage = {
            access_token: res?.data?.token,
            user: res?.data?.user,
            expired: res?.data?.DayToExpired,
            dateExpired: window
              .moment(new Date(), "DD-MM-YYYY")
              .add(res?.data?.DayToExpired, "days")
              .format("YYYY-MM-DD"),
          };
          localStorage.removeItem("session");
          localStorage.setItem("session", JSON.stringify(storage));
          if (storage.user.level === "admin") {
            window.location.href = "/admin/dashboard";
          } else {
            window.location.href = "/agent/dashboard";
          }
        }
      })
      .catch((err) => {
        setOpenSnack(true);
        setMessage(
          err.response
            ? `${err.response.data.message}`
            : "Oops, something wrong with our server, please try again later."
        );
        setLoading(false);
      });
  };
  const handleSubmitLogin = (e) => {
    window.grecaptcha.ready(function () {
      window.grecaptcha
        .execute("6Lcnu3EdAAAAAObE6a1sU-41rpoX8yDczbu9529P", {
          action: "validate_captcha",
        })
        .then(function (token) {
          handleLogin({
            email: e.username,
            password: e.password,
            tokenCaptcha: token,
          });
        });
    });
  };
  React.useEffect(() => {
    loadRecaptchaV3();
  }, []);

  return (
    <Components.Mui.ThemeProvider theme={theme}>
      <Components.Mui.Box
        sx={{
          height: "100vh",
          width: "100vw",
          background: "linear-gradient(180deg, #3f3192 0%, #4f3eb7 100%)",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Components.Mui.CssBaseline />
        <Components.Mui.Box
          display="flex"
          flexDirection="column"
          alignItems="center"
        >
          <img
            src={`${window.BASE_URL}/images/omnix.png`}
            alt="omnix-logo"
            width="150"
            height="70"
          />
          <Components.Mui.Box
            sx={{
              marginTop: "20px",
              width: "300px",
              minWidth: "25vw",
              padding: "25px",
              backgroundColor: "#fff",
              borderRadius: "10px",
              zIndex: 5,
            }}
          >
            <Components.Mui.Typography
              variant="h5"
              sx={{ color: "#3F3192", fontSize: "20px", fontWeight: "bold" }}
            >
              Sign In
            </Components.Mui.Typography>
            <Components.Mui.Box marginBottom="15px" />
            {/* form */}
            <Components.Formik.Formik
              initialValues={{
                username: "",
                password: "",
              }}
              validationSchema={SignInSchema}
              onSubmit={(values) => {
                handleSubmitLogin(values);
              }}
            >
              {({
                errors,
                touched,
                values,
                handleChange,
                handleBlur,
                isValid,
                isSubmitting,
                handleSubmit,
              }) => (
                <Components.Formik.Form onSubmit={handleSubmit}>
                  <Components.Mui.OutlinedInput
                    id="username-input"
                    type="text"
                    name="username"
                    placeholder="Enter Username"
                    startAdornment={
                      <Components.Mui.InputAdornment position="start">
                        <PersonIcon position="start" fontSize="small" />
                      </Components.Mui.InputAdornment>
                    }
                    size="small"
                    value={values.username}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={touched.username && Boolean(errors.username)}
                    fullWidth
                  />
                  {touched.username && errors.username && (
                    <Components.Mui.Typography
                      sx={{
                        margin: "5px 0 0 10px",
                        fontSize: "12px",
                        color: "red",
                      }}
                    >
                      {errors.username}
                    </Components.Mui.Typography>
                  )}
                  <Components.Mui.Box marginBottom="15px" />
                  <Components.Mui.OutlinedInput
                    id="outlined-adornment-password"
                    name="password"
                    type={showPassword ? "text" : "password"}
                    placeholder="Enter Password"
                    startAdornment={
                      <Components.Mui.InputAdornment position="start">
                        <LockIcon position="start" fontSize="small" />
                      </Components.Mui.InputAdornment>
                    }
                    endAdornment={
                      <Components.Mui.InputAdornment position="end">
                        <Components.Mui.IconButton
                          aria-label="toggle password visibility"
                          onClick={() => setShowPassword(!showPassword)}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </Components.Mui.IconButton>
                      </Components.Mui.InputAdornment>
                    }
                    size="small"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={touched.password && Boolean(errors.password)}
                    fullWidth
                  />
                  {touched.password && errors.password && (
                    <Components.Mui.Typography
                      sx={{
                        margin: "5px 0 0 10px",
                        fontSize: "12px",
                        color: "red",
                      }}
                    >
                      {errors.password}
                    </Components.Mui.Typography>
                  )}
                  <Components.Mui.Box marginBottom="15px" />
                  <Components.Mui.Box cs={{ maxWidth: "100%" }}>
                    {/* <ReCAPTCHA
                      sitekey="6Lf2jyYdAAAAANG2d6D9A7ALEbfbfbA5BHQdMZ-n"
                      onChange={(test) => console.log(test)}
                    /> */}
                  </Components.Mui.Box>
                  <Components.Mui.Box marginBottom="15px" />
                  <Components.Mui.Link
                    href="#"
                    sx={{
                      fontFamily: "lato",
                      float: "right",
                      color: "#3F3192",
                      fontSize: "14px",
                      marginBottom: "20px",
                      "&:hover": {
                        color: "#3F3192",
                      },
                    }}
                    onClick={() => window.location.replace("/forgot")}
                  >
                    Forget your password?
                  </Components.Mui.Link>
                  <Components.Mui.Box marginBottom="0" />
                  <Components.Mui.Box
                    sx={{
                      marginTop: "20px",
                      fontSize: "12px",
                      width: "100%",
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <Components.Mui.FormControlLabel
                      control={<Components.Mui.Checkbox defaultChecked />}
                      label="Remember Me"
                    />
                    <Components.Mui.Button
                      sx={{
                        backgroundColor: "#3F3192",
                        color: "#fff",
                        textTransform: "none",
                        height: "37px",
                        width: "116px",
                        "&:hover": {
                          backgroundColor: "#3F3192",
                        },
                      }}
                      type="submit"
                      disabled={!isValid || loading}
                    >
                      {loading ? "Please wait..." : "Sign In"}
                    </Components.Mui.Button>
                  </Components.Mui.Box>
                </Components.Formik.Form>
              )}
            </Components.Formik.Formik>
          </Components.Mui.Box>
        </Components.Mui.Box>
        <Components.Mui.Box
          sx={{
            zIndex: 0,
            minHeight: "100vh",
            minWidth: "100%",
            position: "absolute",
            bottom: "0",
            backgroundPosition: "center",
            backgroundRaepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundImage: `url(${window.BASE_URL}/images/bg-fullwidth.svg)`,
          }}
        />
        <Components.Mui.Box
          sx={{
            zIndex: 0,
            minHeight: "100vh",
            minWidth: "100%",
            position: "absolute",
            bottom: "0",
            backgroundPosition: "center",
            backgroundRaepeat: "no-repeat",
            backgroundSize: "cover",
            transform: "scaleX(-1)",
            backgroundImage: `url(${window.BASE_URL}/images/bg-fullwidth.svg)`,
          }}
        />
      </Components.Mui.Box>
      <Components.Mui.Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        open={openSnack}
        onClose={() => {
          setMessage(false);
          setOpenSnack(false);
        }}
        autoHideDuration={6000}
      >
        <Components.Mui.Alert
          onClose={() => {
            setMessage(false);
            setOpenSnack(false);
          }}
          severity="error"
          sx={{ width: "100%" }}
        >
          {message}
        </Components.Mui.Alert>
      </Components.Mui.Snackbar>
    </Components.Mui.ThemeProvider>
  );
}
export default Root;
