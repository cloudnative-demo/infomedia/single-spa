import React from "react";
// helpers
import Components from "./components/helper";
// icons
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import LockIcon from "@mui/icons-material/Lock";
//import ReCAPTCHA from "react-google-recaptcha";
import theme from "./components/theme";
const SignInSchema = Components.Yup.object().shape({
  password: Components.Yup.string().required("Password required"),
});

function Root(props) {
  const [loading, setLoading] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [message, setMessage] = React.useState(false);
  const [showPassword, setShowPassword] = React.useState(false);
  const [duration, setDuration] = React.useState();
  const storage = JSON.parse(localStorage.getItem("aux"));
  React.useEffect(() => {
    if (!props.isAuthenticated()) {
      window.location.href = "/login";
    } else {
      if (props.isAuthenticated().user.level !== "agent") {
        localStorage.removeItem("session");
        window.location.href = "/login";
      } else {
        if (!localStorage.getItem("aux")) {
          window.location.href = "/agent/dashboard";
        }
      }
    }
  }, []);

  React.useEffect(() => {
    setInterval(() => {
      setDuration(
        window.moment
          .utc(moment(new Date()).diff(moment(new Date(storage.lup))))
          .format("HH:mm:ss")
      );
    }, 1000);
  });

  const handleLogin = (params) => {
    setLoading(true);
    const axios = window.axios.default;
    axios.post(`/auth/auxLogout`, params).then((res) => {
      if (res) {
        setLoading(false);
        localStorage.removeItem("aux");
        window.location.reload(true);
      }
    });
  };

  return (
    <Components.Mui.ThemeProvider theme={theme}>
      <Components.Mui.Box
        sx={{
          height: "100vh",
          width: "100vw",
          background: "linear-gradient(180deg, #3f3192 0%, #4f3eb7 100%)",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Components.Mui.CssBaseline />
        <Components.Mui.Box
          display="flex"
          flexDirection="column"
          alignItems="center"
        >
          <img
            src={`${window.BASE_URL}/images/omnix.png`}
            alt="omnix-logo"
            width="150"
            height="70"
          />
          <Components.Mui.Box
            sx={{
              marginTop: "20px",
              width: "300px",
              minWidth: "25vw",
              padding: "25px",
              backgroundColor: "#fff",
              borderRadius: "10px",
              zIndex: 5,
            }}
          >
            <Components.Mui.Typography
              variant="h5"
              sx={{
                color: "#2D2D2D",
                fontSize: "14px",
                fontWeight: "bold",
                textAlign: "center",
              }}
            >
              Welcome Back,
            </Components.Mui.Typography>
            <Components.Mui.Typography
              variant="h5"
              sx={{
                color: "#3F3192",
                fontSize: "14px",
                fontWeight: "bold",
                textAlign: "center",
              }}
            >
              {props.isAuthenticated().user.name}
            </Components.Mui.Typography>
            <Components.Mui.Box marginBottom="30px" />
            <Components.Mui.Box display="flex" alignItems="center">
              <Components.Mui.Typography
                variant="h5"
                sx={{
                  color: "#2D2D2D",
                  fontSize: "14px",
                  fontWeight: "bold",
                  textAlign: "left",
                }}
              >
                Your Aux Time is
              </Components.Mui.Typography>
              <Components.Mui.Typography
                variant="h5"
                sx={{
                  color: "#3F3192",
                  fontSize: "14px",
                  fontWeight: "bold",
                  marginLeft: "auto",
                }}
              >
                {duration}
              </Components.Mui.Typography>
            </Components.Mui.Box>
            <Components.Mui.Box
              display="flex"
              alignItems="center"
              marginTop={"15px"}
            >
              <Components.Mui.Typography
                variant="h5"
                sx={{
                  color: "#2D2D2D",
                  fontSize: "14px",
                  fontWeight: "bold",
                  textAlign: "left",
                }}
              >
                Reason For Aux is
              </Components.Mui.Typography>
              <Components.Mui.Typography
                variant="h5"
                sx={{
                  color: "#3F3192",
                  fontSize: "14px",
                  fontWeight: "bold",
                  marginLeft: "auto",
                }}
              >
                {storage?.reason}
              </Components.Mui.Typography>
            </Components.Mui.Box>
            <Components.Mui.Box marginBottom="30px" />
            {/* form */}
            <Components.Formik.Formik
              initialValues={{
                password: "",
              }}
              validationSchema={SignInSchema}
              onSubmit={(values) => {
                handleLogin(values);
              }}
            >
              {({
                errors,
                touched,
                values,
                handleChange,
                handleBlur,
                isValid,
                isSubmitting,
                handleSubmit,
              }) => (
                <Components.Formik.Form onSubmit={handleSubmit}>
                  <Components.Mui.OutlinedInput
                    id="outlined-adornment-password"
                    name="password"
                    type={showPassword ? "text" : "password"}
                    placeholder="Enter Password"
                    startAdornment={
                      <Components.Mui.InputAdornment position="start">
                        <LockIcon position="start" fontSize="small" />
                      </Components.Mui.InputAdornment>
                    }
                    endAdornment={
                      <Components.Mui.InputAdornment position="end">
                        <Components.Mui.IconButton
                          aria-label="toggle password visibility"
                          onClick={() => setShowPassword(!showPassword)}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </Components.Mui.IconButton>
                      </Components.Mui.InputAdornment>
                    }
                    size="small"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={touched.password && Boolean(errors.password)}
                    fullWidth
                  />
                  {touched.password && errors.password && (
                    <Components.Mui.Typography
                      sx={{
                        margin: "5px 0 0 10px",
                        fontSize: "12px",
                        color: "red",
                      }}
                    >
                      {errors.password}
                    </Components.Mui.Typography>
                  )}
                  <Components.Mui.Box marginBottom="15px" />
                  <Components.Mui.Box cs={{ maxWidth: "100%" }}>
                    {/* <ReCAPTCHA
                      sitekey="6Lf2jyYdAAAAANG2d6D9A7ALEbfbfbA5BHQdMZ-n"
                      onChange={(test) => console.log(test)}
                    /> */}
                  </Components.Mui.Box>
                  <Components.Mui.Box marginBottom="15px" />
                  <Components.Mui.Button
                    sx={{
                      backgroundColor: "#3F3192",
                      color: "#fff",
                      textTransform: "none",
                      height: "37px",
                      width: "116px",
                      "&:hover": {
                        backgroundColor: "#3F3192",
                      },
                    }}
                    type="submit"
                    disabled={!isValid || loading}
                  >
                    {loading ? "Please wait..." : "Sign In"}
                  </Components.Mui.Button>
                </Components.Formik.Form>
              )}
            </Components.Formik.Formik>
          </Components.Mui.Box>
        </Components.Mui.Box>
        <Components.Mui.Box
          sx={{
            zIndex: 0,
            minHeight: "100vh",
            minWidth: "100%",
            position: "absolute",
            bottom: "0",
            backgroundPosition: "center",
            backgroundRaepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundImage: `url(${window.BASE_URL}/images/bg-fullwidth.svg)`,
          }}
        />
        <Components.Mui.Box
          sx={{
            zIndex: 0,
            minHeight: "100vh",
            minWidth: "100%",
            position: "absolute",
            bottom: "0",
            backgroundPosition: "center",
            backgroundRaepeat: "no-repeat",
            backgroundSize: "cover",
            transform: "scaleX(-1)",
            backgroundImage: `url(${window.BASE_URL}/images/bg-fullwidth.svg)`,
          }}
        />
      </Components.Mui.Box>
      <Components.Mui.Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        open={openSnack}
        onClose={() => {
          setMessage(false);
          setOpenSnack(false);
        }}
        autoHideDuration={6000}
      >
        <Components.Mui.Alert
          onClose={() => {
            setMessage(false);
            setOpenSnack(false);
          }}
          severity="error"
          sx={{ width: "100%" }}
        >
          {message}
        </Components.Mui.Alert>
      </Components.Mui.Snackbar>
    </Components.Mui.ThemeProvider>
  );
}
export default Root;
