import React from "react";
// helpers
import Components from "./components/helper";
// icons
import PersonIcon from "@mui/icons-material/Person";
//import ReCAPTCHA from "react-google-recaptcha";
import theme from "./components/theme";
const ForgotPasswordSchema = Components.Yup.object().shape({
  username: Components.Yup.string()
    .email("Invalid email address format")
    .required("Email is required"),
});

function Root(props) {
  const [loading, setLoading] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [message, setMessage] = React.useState(false);
  const loadRecaptchaV3 = (callback) => {
    const existingScript = document.getElementById("recaptcha");
    if (!existingScript) {
      const script = document.createElement("script");
      script.src = `https://www.google.com/recaptcha/api.js?render=6Lcnu3EdAAAAAObE6a1sU-41rpoX8yDczbu9529P`;
      script.id = "recaptcha";
      script.async = true;
      script.defer = true;
      script.preload = true;
      document.head.appendChild(script);
      script.onload = () => {
        if (callback) callback();
      };
    }
    if (existingScript && callback) callback();
  };
  const handleLogin = (params) => {
    setLoading(true);
    const axios = window.axios.default;
    axios
      .post(`/user/resetPasswordSubmit`, params)
      .then((res) => {
        if (res) {
          setLoading(false);
          setOpenSnack(true);
          setMessage("Reset password link was sent to your email.");
        }
      })
      .catch((err) => {
        setOpenSnack(true);
        setMessage(
          err.response
            ? `${err.response.data.message}`
            : "Oops, something wrong with our server, please try again later."
        );
        setLoading(false);
      });
  };
  const handleSubmitForgot = (e) => {
    window.grecaptcha.ready(function () {
      window.grecaptcha
        .execute("6Lcnu3EdAAAAAObE6a1sU-41rpoX8yDczbu9529P", {
          action: "validate_captcha",
        })
        .then(function (token) {
          handleLogin({
            email: e.username,
          });
        });
    });
  };
  React.useEffect(() => {
    loadRecaptchaV3();
  }, []);

  return (
    <Components.Mui.ThemeProvider theme={theme}>
      <Components.Mui.Box
        sx={{
          height: "100vh",
          width: "100vw",
          background: "linear-gradient(180deg, #3f3192 0%, #4f3eb7 100%)",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Components.Mui.CssBaseline />
        <Components.Mui.Box
          display="flex"
          flexDirection="column"
          alignItems="center"
        >
          <img
            src={`${window.BASE_URL}/images/omnix.png`}
            alt="omnix-logo"
            width="150"
            height="70"
          />
          <Components.Mui.Box
            sx={{
              marginTop: "20px",
              width: "300px",
              minWidth: "25vw",
              padding: "25px",
              backgroundColor: "#fff",
              borderRadius: "10px",
              zIndex: 5,
            }}
          >
            <Components.Mui.Typography
              variant="h5"
              sx={{ color: "#3F3192", fontSize: "20px", fontWeight: "bold" }}
            >
              Forgot Password
            </Components.Mui.Typography>
            <Components.Mui.Typography
              variant="h5"
              sx={{
                color: "#294354",
                fontSize: "12px",
                fontWeight: "normal",
                marginTop: "10px",
              }}
            >
              Enter your registered email below for sent link reset password
            </Components.Mui.Typography>
            <Components.Mui.Box marginBottom="15px" />
            {/* form */}
            <Components.Formik.Formik
              initialValues={{
                username: "",
              }}
              validationSchema={ForgotPasswordSchema}
              onSubmit={(values) => {
                handleSubmitForgot(values);
              }}
            >
              {({
                errors,
                touched,
                values,
                handleChange,
                handleBlur,
                isValid,
                isSubmitting,
                handleSubmit,
              }) => (
                <Components.Formik.Form onSubmit={handleSubmit}>
                  <Components.Mui.OutlinedInput
                    id="username-input"
                    type="text"
                    name="username"
                    placeholder="Enter Email"
                    startAdornment={
                      <Components.Mui.InputAdornment position="start">
                        <PersonIcon position="start" fontSize="small" />
                      </Components.Mui.InputAdornment>
                    }
                    size="small"
                    value={values.username}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={touched.username && Boolean(errors.username)}
                    fullWidth
                  />
                  {touched.username && errors.username && (
                    <Components.Mui.Typography
                      sx={{
                        margin: "5px 0 0 10px",
                        fontSize: "12px",
                        color: "red",
                      }}
                    >
                      {errors.username}
                    </Components.Mui.Typography>
                  )}
                  <Components.Mui.Box marginBottom="15px" />
                  <Components.Mui.Box cs={{ maxWidth: "100%" }}>
                    {/* <ReCAPTCHA
                      sitekey="6Lf2jyYdAAAAANG2d6D9A7ALEbfbfbA5BHQdMZ-n"
                      onChange={(test) => console.log(test)}
                    /> */}
                  </Components.Mui.Box>
                  <Components.Mui.Box marginBottom="15px" />
                  <Components.Mui.Box display="flex" alignItems="center">
                    <Components.Mui.Button
                      sx={{
                        backgroundColor: "#3F3192",
                        color: "#fff",
                        textTransform: "none",
                        height: "37px",
                        width: "116px",
                        "&:hover": {
                          backgroundColor: "#3F3192",
                        },
                      }}
                      type="submit"
                      disabled={!isValid || loading}
                    >
                      {loading ? "Please wait..." : "Send Link Reset"}
                    </Components.Mui.Button>
                    <Components.Mui.Link
                      href="#"
                      sx={{
                        fontFamily: "lato",
                        float: "right",
                        color: "#3F3192",
                        fontSize: "14px",
                        marginLeft: "auto",
                        "&:hover": {
                          color: "#3F3192",
                        },
                      }}
                      onClick={() => window.location.replace("/login")}
                    >
                      Already have an account
                    </Components.Mui.Link>
                  </Components.Mui.Box>
                </Components.Formik.Form>
              )}
            </Components.Formik.Formik>
          </Components.Mui.Box>
        </Components.Mui.Box>
        <Components.Mui.Box
          sx={{
            zIndex: 0,
            minHeight: "100vh",
            minWidth: "100%",
            position: "absolute",
            bottom: "0",
            backgroundPosition: "center",
            backgroundRaepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundImage: `url(${window.BASE_URL}/images/bg-fullwidth.svg)`,
          }}
        />
        <Components.Mui.Box
          sx={{
            zIndex: 0,
            minHeight: "100vh",
            minWidth: "100%",
            position: "absolute",
            bottom: "0",
            backgroundPosition: "center",
            backgroundRaepeat: "no-repeat",
            backgroundSize: "cover",
            transform: "scaleX(-1)",
            backgroundImage: `url(${window.BASE_URL}/images/bg-fullwidth.svg)`,
          }}
        />
      </Components.Mui.Box>
      <Components.Mui.Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        open={openSnack}
        onClose={() => {
          setMessage(false);
          setOpenSnack(false);
        }}
        autoHideDuration={6000}
      >
        <Components.Mui.Alert
          onClose={() => {
            setMessage(false);
            setOpenSnack(false);
          }}
          severity={
            message === "Reset password link was sent to your email."
              ? "success"
              : "error"
          }
          sx={{ width: "100%" }}
        >
          {message}
        </Components.Mui.Alert>
      </Components.Mui.Snackbar>
    </Components.Mui.ThemeProvider>
  );
}
export default Root;
