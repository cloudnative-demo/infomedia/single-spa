#build stage for a Node.js application
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install 
RUN npm install lerna -g
COPY . .
#RUN npm run build
RUN lerna bootstrap
#RUN lerna run
#RUN lerna run build --stream --parallel //bikin out of memory

#production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]