import React from "react";
// helpers
import Components from "./components/helper";
// icons
import LockIcon from "@mui/icons-material/Lock";
//import ReCAPTCHA from "react-google-recaptcha";
import theme from "./components/theme";
const SignInSchemaPasswordExpired = Components.Yup.object().shape({
  newPassword: Components.Yup.string()
    .required("New password is a required field")
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    ),
  confirmPassword: Components.Yup.string()
    .oneOf([Components.Yup.ref("newPassword"), null], "Password doesn't match")
    .required("Password confirmation is a required field"),
});

const SignInSchemaPassword = Components.Yup.object().shape({
  oldPassword: Components.Yup.string().required(
    "Old password is a required field"
  ),
  newPassword: Components.Yup.string()
    .required("New password is a required field")
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    ),
  confirmPassword: Components.Yup.string()
    .oneOf([Components.Yup.ref("newPassword"), null], "Password doesn't match")
    .required("Password confirmation is a required field"),
});

function Root(props) {
  const [loading, setLoading] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [message, setMessage] = React.useState(false);
  const handleChangePassword = (e) => {
    setLoading(true);
    const axios = window.axios.default;
    const postData = { newPassword: e.newPassword };
    if (props.isReset) {
      postData.oldPassword = e.oldPassword;
    }
    axios
      .put(
        `/user/${props.isReset ? "changePassword" : "changePasswordExpired"}`,
        postData
      )
      .then((res) => {
        if (res) {
          setLoading(false);
          const storage = JSON.parse(localStorage.getItem("session"));
          storage.expired = res.data.DayToExpired;
          storage.dateExpired = window
            .moment(new Date(), "DD-MM-YYYY")
            .add(res?.data?.DayToExpired, "days")
            .format("YYYY-MM-DD");
          localStorage.setItem("session", JSON.stringify(storage));
          window.location.reload(true);
        }
      })
      .catch((err) => {
        setOpenSnack(true);
        setMessage(
          err.response
            ? `${err.response.data.message}`
            : "Oops, something wrong with our server, please try again later."
        );
        setLoading(false);
      });
  };

  return (
    <Components.Mui.ThemeProvider theme={theme}>
      <Components.Mui.Box
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        <Components.Mui.Box
          sx={{
            marginTop: "0px",
            width: "25vw",
            padding: "25px",
            backgroundColor: "#fff",
            borderRadius: "10px",
            zIndex: 5,
          }}
        >
          <Components.Mui.Typography
            variant="h5"
            sx={{ color: "#3F3192", fontSize: "20px", fontWeight: "bold" }}
          >
            {props.isReset ? "Change Password" : "Password Expired"}
          </Components.Mui.Typography>
          <Components.Mui.Typography variant="body2" sx={{ fontSize: "12px" }}>
            {props.isReset
              ? "Please create strong password."
              : "Your password expired, please change password before continue."}
          </Components.Mui.Typography>
          <Components.Mui.Box marginBottom="15px" />
          {/* form */}
          <Components.Formik.Formik
            initialValues={{
              oldPassword: "",
              newPassword: "",
              confirmPassword: "",
            }}
            validationSchema={
              props.isReset ? SignInSchemaPassword : SignInSchemaPasswordExpired
            }
            onSubmit={(values) => {
              handleChangePassword(values);
            }}
          >
            {({
              errors,
              touched,
              values,
              handleChange,
              handleBlur,
              isValid,
              isSubmitting,
              handleSubmit,
            }) => (
              <Components.Formik.Form onSubmit={handleSubmit}>
                {props.isReset ? (
                  <>
                    <Components.Mui.OutlinedInput
                      id="outlined-adornment-password"
                      name="oldPassword"
                      type={"password"}
                      placeholder="Enter Old Password"
                      startAdornment={
                        <Components.Mui.InputAdornment position="start">
                          <LockIcon position="start" fontSize="small" />
                        </Components.Mui.InputAdornment>
                      }
                      size="small"
                      value={values.oldPassword}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={touched.oldPassword && Boolean(errors.oldPassword)}
                      fullWidth
                    />
                    {touched.oldPassword && errors.oldPassword && (
                      <Components.Mui.Typography
                        sx={{
                          margin: "5px 0 0 10px",
                          fontSize: "12px",
                          color: "red",
                        }}
                      >
                        {errors.oldPassword}
                      </Components.Mui.Typography>
                    )}
                    <Components.Mui.Box marginBottom="15px" />
                  </>
                ) : null}
                <Components.Mui.OutlinedInput
                  id="outlined-adornment-password"
                  name="newPassword"
                  type={"password"}
                  placeholder="Enter New Password"
                  startAdornment={
                    <Components.Mui.InputAdornment position="start">
                      <LockIcon position="start" fontSize="small" />
                    </Components.Mui.InputAdornment>
                  }
                  size="small"
                  value={values.newPassword}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={touched.newPassword && Boolean(errors.newPassword)}
                  fullWidth
                />
                {touched.newPassword && errors.newPassword && (
                  <Components.Mui.Typography
                    sx={{
                      margin: "5px 0 0 10px",
                      fontSize: "12px",
                      color: "red",
                    }}
                  >
                    {errors.newPassword}
                  </Components.Mui.Typography>
                )}
                <Components.Mui.Box marginBottom="15px" />
                <Components.Mui.OutlinedInput
                  id="outlined-adornment-password"
                  name="confirmPassword"
                  type={"password"}
                  placeholder="Enter Confirmation Password"
                  startAdornment={
                    <Components.Mui.InputAdornment position="start">
                      <LockIcon position="start" fontSize="small" />
                    </Components.Mui.InputAdornment>
                  }
                  size="small"
                  value={values.confirmPassword}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={
                    touched.confirmPassword && Boolean(errors.confirmPassword)
                  }
                  fullWidth
                />
                {touched.confirmPassword && errors.confirmPassword && (
                  <Components.Mui.Typography
                    sx={{
                      margin: "5px 0 0 10px",
                      fontSize: "12px",
                      color: "red",
                    }}
                  >
                    {errors.confirmPassword}
                  </Components.Mui.Typography>
                )}
                <Components.Mui.Box marginBottom="15px" />
                <Components.Mui.Box cs={{ maxWidth: "100%" }}>
                  {/* <ReCAPTCHA
                      sitekey="6Lf2jyYdAAAAANG2d6D9A7ALEbfbfbA5BHQdMZ-n"
                      onChange={(test) => console.log(test)}
                    /> */}
                </Components.Mui.Box>
                <Components.Mui.Box marginBottom="15px" />
                <Components.Mui.Box
                  display="flex"
                  alignItems="center"
                  justifyContent={"center"}
                >
                  <Components.Mui.Button
                    variant="contained"
                    type="submit"
                    disabled={!isValid || loading}
                    sx={{
                      marginRight: props.isReset ? "10px" : "unset",
                      width: props.isReset ? "auto" : "100%",
                    }}
                  >
                    {loading ? "Please wait..." : "Submit"}
                  </Components.Mui.Button>
                  {props.isReset ? (
                    <Components.Mui.Button
                      variant="outlined"
                      onClick={() => props.handleClose()}
                    >
                      Cancel
                    </Components.Mui.Button>
                  ) : null}
                </Components.Mui.Box>
              </Components.Formik.Form>
            )}
          </Components.Formik.Formik>
        </Components.Mui.Box>
      </Components.Mui.Box>
      <Components.Mui.Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        open={openSnack}
        onClose={() => {
          setMessage(false);
          setOpenSnack(false);
        }}
        autoHideDuration={6000}
      >
        <Components.Mui.Alert
          onClose={() => {
            setMessage(false);
            setOpenSnack(false);
          }}
          severity="error"
          sx={{ width: "100%" }}
        >
          {message}
        </Components.Mui.Alert>
      </Components.Mui.Snackbar>
    </Components.Mui.ThemeProvider>
  );
}
export default Root;
