import React from "react";
// helpers
import Components from "./components/helper";
//import ReCAPTCHA from "react-google-recaptcha";
import theme from "./components/theme";
const AuksFormSchema = Components.Yup.object().shape({
  reason: Components.Yup.string().required("Reason is required"),
});

function Root(props) {
  const [loading, setLoading] = React.useState(false);
  const [openSnack, setOpenSnack] = React.useState(false);
  const [message, setMessage] = React.useState(false);
  const [auks, setAuks] = React.useState([]);
  const axios = window.axios.default;
  React.useEffect(() => {
    axios.get("/aux/list").then((res) => setAuks(res?.data ?? []));
  }, []);
  const handleUpdate = (params) => {
    setLoading(true);
    axios.post(`/auth/auxLogin`, params).then((res) => {
      if (res) {
        localStorage.setItem(
          "aux",
          JSON.stringify({
            reason: params.reason,
            lup: new Date(),
          })
        );
        window.location.reload(true);
      }
    });
  };

  return (
    <Components.Mui.ThemeProvider theme={theme}>
      <Components.Formik.Formik
        enableReinitialize
        initialValues={{ reason: "" }}
        validationSchema={AuksFormSchema}
        onSubmit={(values) => {
          handleUpdate(values);
        }}
      >
        {({
          errors,
          touched,
          values,
          handleChange,
          handleBlur,
          isValid,
          isSubmitting,
          setFieldValue,
          handleSubmit,
        }) => (
          <Components.Formik.Form onSubmit={handleSubmit}>
            <Components.Mui.FormControl
              size="small"
              sx={{ width: "100%", marginBottom: "10px" }}
              error={touched.reason && Boolean(errors.reason)}
            >
              <Components.Mui.InputLabel id="reason">
                Reason
              </Components.Mui.InputLabel>
              <Components.Mui.Select
                labelId="reason"
                id="reason"
                label="Reason"
                onChange={(e) => setFieldValue("reason", e.target.value)}
                onBlur={handleBlur}
                value={values.reason}
              >
                {auks.length > 0
                  ? auks.map((a, b) => (
                      <Components.Mui.MenuItem value={a.name} key={b}>
                        {a.name}
                      </Components.Mui.MenuItem>
                    ))
                  : null}
              </Components.Mui.Select>
              <Components.Mui.FormHelperText>
                {errors.reason}
              </Components.Mui.FormHelperText>
            </Components.Mui.FormControl>
            <Components.Mui.Box
              display="flex"
              alignItems="center"
              justifyContent={"center"}
            >
              <Components.Mui.Button
                size="small"
                variant="contained"
                type="submit"
                disabled={!isValid || loading}
                sx={{ marginRight: "10px" }}
              >
                {loading ? "Please wait..." : "Submit"}
              </Components.Mui.Button>
              <Components.Mui.Button
                variant="outlined"
                onClick={() => props.handleClose()}
              >
                Cancel
              </Components.Mui.Button>
            </Components.Mui.Box>
          </Components.Formik.Form>
        )}
      </Components.Formik.Formik>
      <Components.Mui.Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        open={openSnack}
        onClose={() => {
          setMessage(false);
          setOpenSnack(false);
        }}
        autoHideDuration={6000}
      >
        <Components.Mui.Alert
          onClose={() => {
            setMessage(false);
            setOpenSnack(false);
          }}
          severity={message === "Aux status success." ? "success" : "error"}
          sx={{ width: "100%" }}
        >
          {message}
        </Components.Mui.Alert>
      </Components.Mui.Snackbar>
    </Components.Mui.ThemeProvider>
  );
}
export default Root;
